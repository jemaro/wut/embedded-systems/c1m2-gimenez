# C1M2-GIMENEZ

You will write a makefile that can compile multiple source files and support
two platform targets. You will be given a folder structure and a set of source
files.

make commands should be executed from src

```
cd ./src
make build
```